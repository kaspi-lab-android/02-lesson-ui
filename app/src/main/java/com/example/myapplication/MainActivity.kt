package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewButton.setOnClickListener {
            startActivity(Intent(this, ViewActivity::class.java))
        }

        linearLayoutButton.setOnClickListener {
            startActivity(Intent(this, LinearLayoutActivity::class.java))
        }

        relativeLayoutButton.setOnClickListener {
            startActivity(Intent(this, RelativeLayoutActivity::class.java))
        }

        frameLayoutButton.setOnClickListener {
            startActivity(Intent(this, FrameLayoutActivity::class.java))
        }

        coordinatorLayoutButton.setOnClickListener {
            startActivity(Intent(this, CoordinatorLayoutActivity::class.java))
        }

        constraintLayoutButton.setOnClickListener {
            startActivity(Intent(this, ConstraintLayoutActivity::class.java))
        }

        gridLayoutButton.setOnClickListener {
            startActivity(Intent(this, GridLayoutActivity::class.java))
        }

        tableLayoutButton.setOnClickListener {
            startActivity(Intent(this, TableLayoutActivity::class.java))
        }

        paddingButton.setOnClickListener {
            startActivity(Intent(this, PaddingActivity::class.java))
        }
    }
}
